import { createApp } from "vue";
import App from "./App.vue";

//引入Pinia
import { createPinia } from "pinia";
const pinia = createPinia();

//引入路由
import router from "./router/index";

//引入axios
import request from "./axios/index";

//引入ElementIcon
import * as ElementPlusIconsVue from "@element-plus/icons-vue";
import "element-plus/dist/index.css";

const app = createApp(App);

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component as any);
}
app.use(router as any);
app.use(pinia as any);

app.mount("#app");
