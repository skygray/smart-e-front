import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/smart-e/admin/login",
    name: "login",
    meta: {
      title: "登录",
    },
    component: () => import("../view/login.vue"),
  },
  {
    path: "/smart-e/admin/",
    meta: {
      title: "首页",
    },
    redirect: (to) => {
      return "/smart-e/admin/index";
    },
  },
  {
    path: "/smart-e/admin/index",
    name: "index",
    meta: {
      title: "首页",
    },
    component: () => import("../view/index.vue"),
    children: [
      {
        path: "/smart-e/admin/index/notice",
        name: "notice",
        meta: {
          title: "公告管理",
        },
        component: () => import("../view/notice.vue"),
        children: [
          {
            path: "/smart-e/admin/index/repair/ChangeIndex",
            name: "changeNoticeIndex",
            meta: {
              title: "修改公告层级",
            },
            component: () => import("../components/ChangeIndex.vue"),
          },
          {
            path: "/smart-e/admin/index/repair/AddForm",
            name: "AddForm",
            meta: {
              title: "添加公告",
            },
            component: () => import("../components/AddForm.vue"),
          },
        ],
      },
      {
        path: "/smart-e/admin/index/repair",
        name: "repair",
        meta: {
          title: "维修管理",
        },
        component: () => import("../view/repair.vue"),
      },
      {
        path: "/smart-e/admin/index/complaint",
        name: "complaint",
        meta: {
          title: "投诉管理",
        },
        component: () => import("../view/complaint.vue"),
      },
      {
        path: "/smart-e/admin/index/lease",
        name: "lease",
        meta: {
          title: "租赁管理",
        },
        component: () => import("../view/lease.vue"),
        children: [
          {
            path: "/smart-e/admin/index/lease/EditForm",
            name: "EditForm",
            meta: {
              title: "编辑租赁信息",
            },
            component: () => import("../components/EditForm.vue"),
          },
        ],
      },
      {
        path: "/smart-e/admin/index/visitor",
        name: "visitor",
        meta: {
          title: "访客管理",
        },
        component: () => import("../view/visitor.vue"),
      },
      {
        path: "/smart-e/admin/index/messBord",
        name: "messBord",
        meta: {
          title: "留言管理",
        },
        component: () => import("../view/messBord.vue"),
        children: [
          {
            path: "/smart-e/admin/index/messBord/ChangeIndex",
            name: "changeMessBordIndex",
            meta: {
              title: "修改留言层级",
            },
            component: () => import("../components/ChangeIndex.vue"),
          },
        ],
      },
      {
        path: "/smart-e/admin/index/auth",
        name: "auth",
        meta: {
          title: "身份认证",
        },
        component: () => import("../view/auth.vue"),
        children: [
          {
            path: "/smart-e/admin/index/auth/verify",
            name: "chooseStatus",
            meta: {
              title: "审核",
            },
            component: () => import("../components/ChooseStatus.vue"),
          },
        ],
      },
      {
        path: "/smart-e/admin/index/user",
        name: "user",
        meta: {
          title: "用户管理",
        },
        component: () => import("../view/user.vue"),
        children: [
          {
            path: "/smart-e/admin/index/user/EditUser",
            name: "EditUser",
            meta: {
              title: "编辑用户信息",
            },
            component: () => import("../components/EditUser.vue"),
          },
        ],
      },
      {
        path: "/smart-e/admin/index/system",
        name: "system",
        meta: {
          title: "登录日志",
        },
        component: () => import("../view/system.vue"),
      },
    ],
  },
  {
    path: "/smart-e/admin/index/previewPhoto",
    name: "previewPhoto",
    meta: {
      title: "图片预览",
    },
    component: () => import("../components/Preview.vue"),
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (!sessionStorage.getItem("token")) {
    if (to.name == "login") {
      next();
    } else {
      next("/smart-e/admin/login");
    }
  } else {
    next();
  }
});

export default router;
