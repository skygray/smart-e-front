export interface Notice {
  id: number;
  notice_date: string;
  notice_describe: string;
  notice_img: string;
  notice_status: number;
  notice_title: string;
  notice_top: number;
  tag: string;
}

export interface Repair {
  id: number;
  nickName: string;
  avatarUrl: string;
  repair_title: string;
  repair_img: string;
  repair_describe: string;
  repair_tel: string;
  repair_type: number;
  repair_status: number;
  repair_date: string;
  tag: string;
}

export interface Complaint {
  id: number;
  nickName: string;
  avatarUrl: string;
  complaint_title: string;
  complaint_describe: string;
  complaint_status: number;
  complaint_date: string;
  tag: string;
}

export interface Lease {
  id: number;
  building_name: string;
  house_plate: string;
  house_area: string;
  lease_title: string;
  lease_price: number;
  lease_imageUrl: string;
  lease_introduce: string;
  lease_contacts: string;
  lease_tel: string;
  lease_keyword: string;
  lease_time: string;
  lease_status: number;
  lease_index: string;
  tag: string;
}

export interface Visitor {
  id: number;
  building_name: string;
  house_plate: string;
  visitor_name: string;
  visitor_tel: string;
  visitor_mine_status: number;
  visitor_house_status: number;
  visitor_status: number;
  visitor_date: string;
  tag: string;
}

export interface Auth {
  id: number;
  nickName: string;
  building_name: string;
  house_plate: string;
  owner_name: string;
  owner_tel: string;
  owner_email: string;
  owner_auth: string;
  owner_status: number;
  owner_date: string;
  tag: string;
}

export interface User {
  id: number;
  openid: string;
  nickName: string;
  avatarUrl: string;
  country: string;
  province: string;
  city: string;
  role: number;
}

export interface System {
  id: number;
  login_ip: string;
  login_time: string;
  tag: string;
}

export interface MessBord {
  id: number;
  mess_author: string;
  mess_avatarUrl: string;
  mess_content: string;
  mess_date: string;
  mess_status: number;
  mess_index: number;
}
