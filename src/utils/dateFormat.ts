//格式化时间
function timeFormat(data: Array<any>, inKey: string): Array<any> {
  for (let i = 0; i < data.length; i++) {
    for (let key in data[i]) {
      if (key == inKey) {
        const OldTime = data[i][key];
        data[i][key] = timeFormatSeconds(OldTime);
      }
    }
  }
  return data;
}

function timeFormatSeconds(time: string): string {
  const d = time ? new Date(time) : new Date();
  let year: string | number = d.getFullYear();
  let month: string | number = d.getMonth() + 1;
  let day: string | number = d.getDate();
  let hours: string | number = d.getHours();
  let min: string | number = d.getMinutes();
  let seconds: string | number = d.getSeconds();

  if (month < 10) month = "0" + month;
  if (day < 10) day = "0" + day;
  if (hours < 0) hours = "0" + hours;
  if (min < 10) min = "0" + min;
  if (seconds < 10) seconds = "0" + seconds;

  return (
    year + "-" + month + "-" + day + " " + hours + ":" + min + ":" + seconds
  );
}

export { timeFormat };
