//隐藏标签 鼠标进入触发显示
function hideElement(
  triggerELement: HTMLElement,
  element1: HTMLElement,
  element2: HTMLElement
) {
  const elementList: HTMLElement[] = [element1, element2];
  triggerELement.addEventListener("mouseenter", function () {
    for (let i = 0; i < elementList.length; i++) {
      elementList[i].style.visibility = "visible";
    }
  });
}

export { hideElement };
