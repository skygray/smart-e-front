//获取浏览器高度
export function PageHeight(height: number): number {
  if (height <= 484) {
    return 3;
  } else if (484 < height && height <= 624) {
    return 4;
  } else if (624 < height && height <= 800) {
    return 7;
  }
  return 10;
}
