import { Lease } from "../types/listTypes";

//将字符串中含有逗号转换成数组
function toArray(data: Array<any>, inKey: string): Array<any> {
  for (let i = 0; i < data.length; i++) {
    for (let key in data[i]) {
      if (key === inKey) {
        data[i][key] = data[i][key].split(",");
      }
    }
  }
  return data;
}

export { toArray };
